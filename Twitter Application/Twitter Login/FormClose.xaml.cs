﻿using System.Windows;
using System.Windows.Controls;

namespace Twitter_Login
{
    /// <summary>
    /// Interaction logic for FormClose.xaml
    /// </summary>
    public partial class FormClose : Window
    {
        public FormClose()
        {
            InitializeComponent();
        }

        private void Grid_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {

            this.Hide();
            FormClose f1 = new FormClose();
            f1.Show();


        }
    }
}
