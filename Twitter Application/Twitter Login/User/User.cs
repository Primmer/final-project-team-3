﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitter_Login
{
    class User
    {
        public string TwitterUsername { get; set; }
        public string TwitterPassword { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Gender { get; set ;}
        public string DateOfBirth { get; set; }

        private void SetGender() { }

        private void SetProfilePicture() { }
        private void SetUserDescription() { }
        private void SetDateOfBirth() { }
    }
}
