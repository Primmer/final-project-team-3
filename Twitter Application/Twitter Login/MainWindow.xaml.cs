﻿using System.Configuration;
using System.Windows;
using Twitter_Login.Properties;

namespace Twitter_Login
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            string name = Settings.Default.UserName;
          
            PasswordBox1.Password = "yourpassword";

            if (txtUserName.Text == name && PasswordBox1.Password == "yourpassword")
            {
                this.Hide();
                TweetWindow dash = new TweetWindow();
                dash.Show();
            }

            else if (txtUserName.Text != name || PasswordBox1.Password != "yourpassword")
            {
                MessageBox.Show("Invalid Username or Password");
            }

        }
    }
}
