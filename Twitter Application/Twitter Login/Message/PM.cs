﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitter_Login
{
    class PM
    {

        public Content Contents { get; set; }
        public MessageUsers Participants { get; set; }

        public void SendMessage()
        {
            Console.WriteLine("Sending {0} to {1} from {2} with subject {3}", Contents.Message, Participants.Recipient,
                Participants.Sender, Contents.Subject);
        }

    }
}
