﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitter_Login
{
    class Message : User
    {
        public string Text { get; set; }
        public string CharacterLimit { get; set; }
        public string User { get; set; }

        public Message()
        {

        } 
        public Message(int characterLimit)
        {
            CharacterLimit = CharacterLimit;
        }
        public string SendTweet()
        {
            return Text;
        }
        public string DeleteTweet()
        {
            return Text;
        } 
    }
}
