﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tweetinvi;


namespace Twitter_Login
{
    /// <summary>
    /// Interaction logic for TweetWindow.xaml
    /// </summary>
    public partial class TweetWindow : Window
    {
        public TweetWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtSendTweet.MaxLength = 140;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TweetSender tweetSender = new TweetSender();
            tweetSender.SendTweet(txtSendTweet.Text);
            txtSendTweet.Text = ""; 
        }
    }
}
