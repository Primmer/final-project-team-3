﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Twitter_Login.Address_Book
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }
        List<Person> people = new List<Person>();

        private void AdressBook_Load(object sender, RoutedEventArgs e)
        {

            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            if (!Directory.Exists(path + "\\Address Book"))
                Directory.CreateDirectory(path + "\\Address Book");
            if (!File.Exists(path + "\\Address Book \\settings.xml"))
                File.Create((path + "\\Address Book \\settings.xml"));

        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Person p = new Person
            {
                FullName = txtFullName.Text,
                TwitterHandle = txtHandle.Text
            };
            people.Add(p);
            listView1.Items.Add(p.FullName);
            txtFullName.Text = "";
            txtHandle.Text = "";

        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //txtFullName.Text = people[listView1.SelectedItems[0].Index].FullName;
            //txtHandle.Text = people[listView1.SelectedItems[0].Index].TwitterHandle;
        }

        private void TxtFullName_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAdd_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void btnSendMessage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnBlock_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    internal class Person
    {
        public string FullName { get; set; }
        public string TwitterHandle { get; set; }
    }


}


